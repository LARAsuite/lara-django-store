"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_store urls *

:details: lara_django_store urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_store"
