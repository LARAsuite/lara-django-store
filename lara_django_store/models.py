"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_store models *

:details: lara_django_store database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
import datetime
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from django.db import models


from lara_django_base.models import Namespace, ItemStatus, Currency, Location, Tag
from lara_django_people.models import Entity


class ItemInstanceAbstr(models.Model):
    """ Abstract Item Instance Model fitting for parts, devices, labware, chemicals, organisms, etc.
    """
    # item = models.ForeignKey(Part, related_name='%(app_label)s_%(class)s_parts_related',
    #                          related_query_name="%(app_label)s_%(class)s_parts_related_query",
    #                          on_delete=models.CASCADE, null=True, blank=True)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of part")
    name = models.TextField(blank=True, help_text="short item name")
    name_full = models.TextField(
        blank=True, help_text="long item name")
    barcode1D = models.TextField(
        help_text="1D barcode of location", default=uuid.uuid4)
    # -> JSON ?
    barcode2D = models.TextField(
        blank=True, null=True, help_text="2D barcode of location - binary")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="UUID of the item")
    URL = models.URLField(
        blank=True,  null=True, help_text="Universal Resource Locator - URL")
    handle = models.URLField(
        blank=True, null=True,  unique=True, help_text="handle URI")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    registration_no = models.TextField(
        blank=True, help_text="can be used for internal registrations in a labstore")
    vendor = models.ForeignKey(Entity, related_name="%(app_label)s_%(class)s_vendors_related",
                               related_query_name="%(app_label)s_%(class)s_vendors_related_query",
                               on_delete=models.CASCADE, blank=True, null=True,
                               help_text="vendor of the device")
    product_id = models.TextField(
        blank=True, help_text="vendor product id")
    serial_no = models.TextField(
        blank=True, help_text="part/devices serial number")
    service_no = models.TextField(
        blank=True, help_text="service number or tag")
    manufacturing_date = models.DateTimeField(
        default=datetime.datetime(1, 1, 1), help_text="date of manufaction")
    # price - try also https://github.com/django-money/django-money
    price = models.DecimalField(blank=True,  null=True, max_digits=8, decimal_places=2,
                                help_text="price of the part in default currency/EURO")
    currency = models.ForeignKey(Currency, related_name="%(app_label)s_%(class)s_currencies_related",
                                 related_query_name="%(app_label)s_%(class)s_currencies_related_query",
                                 on_delete=models.CASCADE, blank=True, null=True,
                                 help_text="currency of the item price")
    # toll, discount
    purchase_date = models.DateTimeField(default=datetime.datetime(1, 1, 1), help_text="Date of item purchase")
    date_delivery = models.DateTimeField(default=timezone.now, help_text="Date of Item delivery = inventarisation date")
    end_of_warranty_date = models.DateTimeField(default=datetime.datetime(1, 1, 1), help_text="")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_locations_related',
                                 related_query_name="%(app_label)s_%(class)s_locations_related_query",
                                 on_delete=models.CASCADE, null=True, blank=True,
                                 help_text="location of container")

    hash_SHA256 = models.CharField(max_length=256, blank=True, null=True,
                                   help_text="SHA256 hash of item for identity/duplicate checking")

    literature = models.URLField(null=True, blank=True, help_text="literature regarding this item instance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this part instance")

    tags = models.ManyToManyField(Tag, blank=True, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  help_text="tags")
    description = models.TextField(
        blank=True, null=True, help_text="description of part instance")

    def warrantyLeft(self, day_interval=7):
        """ choose day_interval = 7 for weeks
        """
        now = datetime.now()
        # datetime object is required for substraction
        warranty_end = datetime.combine(
            self.end_of_warranty_date, datetime.min.time())
        warranty_left = warranty_end - now

        return warranty_left.days / day_interval

    warrantyLeft.admin_order_field = 'end_of_warranty_date'
    warrantyLeft.short_description = "Warranty left [weeks]"

    def endOfWarrantySoon(self):
        now = datetime.now()
        warranty_end = datetime.combine(
            self.end_of_warranty_date, datetime.min.time())
        warranty_left = warranty_end - now

        return warranty_left < datetime.timedelta(weeks=12)

    endOfWarrantySoon.admin_order_field = 'end_of_warranty_date'
    endOfWarrantySoon.boolean = True
    endOfWarrantySoon.short_description = 'Warranty ends soon ?'

    def inWarrantyPeriod(self):
        today = datetime.datetime.today()

        return (self.end_of_warranty_date > today)

    inWarrantyPeriod.admin_order_field = 'end_of_warranty_date'
    inWarrantyPeriod.boolean = True
    inWarrantyPeriod.short_description = 'Still Warranty ?'

    # booking should be done from Calendar side
    # ~ booking_events = models.ManyToManyField('CalendarEvent', related_name='device_booking_event', blank=True)
    # ~ maintenance_events = models.ManyToManyField('CalendarEvent', related_name='device_maintenace_event', blank=True)
    # device state
    # maintainer group

    # def __str__(self):
    #     return self.part.name_full or ""

    # def __repr__(self):
    #     return self.part.name_full or ""

    class Meta:
        abstract = True


class OrderState(models.Model):
    """ States of Order Process: requested, quote, ordered, arrived"""
    order_state_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    state = models.TextField(unique=True, blank=True,
                             help_text="state of the order, to_order, pending, ordered, recieved ...")

    def __str__(self):
        return self.state or ''


# validation in django administration
def validate_quantity(value):
    if value == 0 or value > 999:
        raise ValidationError('%s - is incorrect number, 1-999 only!' % value)


class OrderItemAbstr(models.Model):
    """ Abstract class for defining items to be ordered. This is required to store quantities, prices, etc. per item """

    quantity = models.PositiveIntegerField(validators=[validate_quantity], default=1)

    item_price_net = models.DecimalField(max_digits=10, decimal_places=2,
                                         default=0.0, help_text="net price for this item")
    item_price_gross = models.DecimalField(max_digits=10, decimal_places=2,
                                           default=0.0, help_text="gross price for this item")
    item_shipping_price = models.DecimalField(max_digits=10, decimal_places=2,
                                              default=0.0, help_text="shipping price for this item")
    item_price_tax = models.DecimalField(max_digits=10, decimal_places=2,
                                         default=0.0, help_text="tax rate for this item")
    item_toll = models.DecimalField(max_digits=10, decimal_places=2, default=0.0, help_text="toll for this item")
    toll_number = models.TextField(
        blank=True, null=True,  help_text="description of data type")
    item_discount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0,
                                        help_text="discount for this item")
    total_price_net = models.DecimalField(max_digits=10, decimal_places=2, default=0.0,
                                          help_text="total price for this item")

    class Meta:
        abstract = True


class ItemOrderAbstr(models.Model):
    """ Abstract class for Odering Items (OrderItems).
    """
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of order ")
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="name of the order")

    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")

    user_ordered = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_users_ordered_related',
                                     related_query_name="%(app_label)s_%(class)s_users_ordered_related_query",
                                     on_delete=models.CASCADE, null=True, blank=True, help_text="user who ordered the item")
    user_ordering = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_users_ordering_related',
                                      related_query_name="%(app_label)s_%(class)s_users_ordering_related_query",
                                      on_delete=models.CASCADE, null=True, blank=True, help_text="user who is ordering the item (from an external resource like a company)")
    order_date = models.DateTimeField(auto_now=True, blank=True, null=True, help_text="date of order")
    order_id_internal = models.TextField(blank=True, null=True, help_text="internal id of order")
    order_id_external = models.TextField(blank=True, null=True, help_text="external id of order")
    order_barcode = models.TextField(
        blank=True, null=True, help_text="barcode of order, can be used for tracking or in-house delivery")

    order_company = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_order_companies_related',
                                      related_query_name="%(app_label)s_%(class)s_order_companies_related_query",
                                      on_delete=models.CASCADE, null=True, blank=True, help_text="company the item was ordered at")
    order_quote_id = models.TextField(blank=True, null=True, help_text="quote id of order")
    order_quote_date = models.DateTimeField(blank=True, null=True, help_text="date of quote")
    # order_quotes = models.ManyToManyField(Quote, related_name='%(app_label)s_%(class)s_order_quotes_related',
    #                                         related_query_name="%(app_label)s_%(class)s_order_quotes_related_query",
    #                                         blank=True, help_text="quotes for this order")
    order_invoice_id = models.TextField(blank=True, null=True, help_text="invoice id of order")
    order_invoice_date = models.DateTimeField(blank=True, null=True, help_text="date of invoice")
    # order_invoices = models.ManyToManyField(Invoice, related_name='%(app_label)s_%(class)s_order_invoices_related',
    #                                         related_query_name="%(app_label)s_%(class)s_order_invoices_related_query",
    #                                         blank=True, help_text="invoices for this order")

    # shipping
    shipping_company = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_shipping_companies_related',
                                         related_query_name="%(app_label)s_%(class)s_shipping_companies_related_query",
                                         on_delete=models.CASCADE, null=True, blank=True, help_text="company shipping the item")
    shipping_tracking_id = models.TextField(blank=True, null=True, help_text="tracking id of shipping")
    shipping_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True,
                                         null=True, help_text="price of shipping")
    # add budget
    budget_URL = models.URLField(
        blank=True,  null=True, max_length=512, help_text="Link to budget")
    order_pay_date = models.DateTimeField(auto_now=True, blank=True, null=True, help_text="date of order payment")

    order_total_price_net = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True, help_text="total price of order net")

    order_total_price_gross = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True, help_text="total price of order gross")

    order_total_price_tax = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True, help_text="total price of order TAX/VAT")

    order_total_price_toll = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True, help_text="total price of order toll")

    order_total_price_currency = models.ForeignKey(Currency, related_name='%(app_label)s_%(class)s_order_total_price_currencies_related',
                                                   related_query_name="%(app_label)s_%(class)s_order_total_price_currencies_related_query",
                                                   on_delete=models.CASCADE, null=True, blank=True, help_text="currency of order total price")

    # ~ order_state # requested, quote, ordered, arrived
    order_state = models.ForeignKey(OrderState, related_name='%(app_label)s_%(class)s_order_states_related',
                                    related_query_name="%(app_label)s_%(class)s_order_states_related_query",
                                    on_delete=models.CASCADE, null=True, blank=True,
                                    help_text="state of the order")

    def __str__(self):
        return self.name_full or ''

    class Meta:
        abstract = True


class ItemBasketAbstr(models.Model):
    """ Abstract class for collecting OrderItems in a basket or wishlist for (re-)ordering
        :note: an item stash/local store can be generated by subclassing ItemBasketAbstr class and 
               adding a location
    """
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True, help_text="namespace of the basket")
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="full name of the basket")
    user = models.ForeignKey(Entity, related_name='%(app_label)s_%(class)s_users_related',
                             related_query_name="%(app_label)s_%(class)s_users_related_query",
                             on_delete=models.CASCADE, null=True, blank=True, help_text="user owning the basket")
    date_created = models.DateTimeField(default=timezone.now, blank=True, null=True, help_text="date of order")
    date_modified = models.DateTimeField(default=timezone.now, blank=True, null=True, help_text="date of order")

    class Meta:
        abstract = True
