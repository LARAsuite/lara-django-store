"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_store admin *

:details: lara_django_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_store >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import OrderState


@admin.register(OrderState)
class OrderStateAdmin(admin.ModelAdmin):
    list_display = ('order_state_id', 'state')
